
local Map = require 'map'

local IsoView = require 'class' ()

function IsoView:_init(data)
  self.map = Map(data)
end

function IsoView:update()
  self.map:update()
end

function IsoView:draw()
  local g = love.graphics
  local w, h = g.getDimensions()
  g.push()
  g.translate(w/2, h/2)
  self.map:draw()
  g.pop()
end

return IsoView

