
-- luacheck: globals love

local TileSet     = require 'tileset'
local PathTracker = require 'pathtracker'
local Sprite      = require 'sprite'

local Map = require 'class' ()

function Map:_init(data)
  self.tileset = TileSet(data.tilesets[1])
  self.layers = data.layers
  self.tilewidth = data.tilewidth
  self.tileheight = data.tileheight
  self.camera_tracker = PathTracker(data.layers, 'camera')
  self.sprites = Sprite.loadAll(data.layers)
  local bgred, bggreen, bgblue = unpack(data.backgroundcolor)
  love.graphics.setBackgroundColor(bgred/256, bggreen/256, bgblue/256)
end

function Map:worldToScreen(x, y, z)
  -- | w -w  0 |
  -- | h  h  1 |
  local tw, th = self.tilewidth, self.tileheight
  local isox = (x - y) * math.floor(tw/2)
  local isoy = (x + y) * math.floor(th/2) + z
  return isox, isoy
end

function Map:update()
  self.camera_tracker:update()
  for _, sprite in pairs(self.sprites) do
    sprite:update()
  end
end

function Map:draw()
  local g = love.graphics
  g.push()
  local cx, cy = self:worldToScreen(self.camera_tracker:getPosition())
  g.translate(-cx, -cy)
  for _, layer in ipairs(self.layers) do
    if layer.type == 'tilelayer' then
      for x = 0, layer.width - 1 do
        for y = 0, layer.height - 1 do
          local idx = y * layer.width + x + 1
          local tile_id = layer.data[idx]
          if tile_id > 0 then
            -- | w -w |
            -- | h  h |
            local isox, isoy = self:worldToScreen(x, y, layer.offsety)
            self.tileset:drawTile(tile_id, isox, isoy)
          end
        end
      end
    elseif layer.type == 'objectgroup' then
      for _, object in ipairs(layer.objects) do
        if object.type == 'sprite' then
          local sprite = self.sprites[object.id]
          local x, y = sprite:getPosition()
          x, y = x + .5, y + .5
          sprite:draw(self:worldToScreen(x, y, layer.offsety))
        end
      end
    end
  end
  g.pop()
end

return Map

