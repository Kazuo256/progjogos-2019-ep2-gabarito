
-- luacheck: globals love

local PathTracker = require 'class' ()

function PathTracker:_init(layers_data, typename)
  self.x = 0
  self.y = 0
  self.steps = {}
  for _, layer in ipairs(layers_data) do
    if layer.type == 'objectgroup' then
      for _, obj in ipairs(layer.objects) do
        if obj.type == typename then
          self.steps[tonumber(obj.name)] = {
            x = (obj.x + obj.width / 2) / obj.width,
            y = (obj.y + obj.height / 2) / obj.height,
            z = layer.offsety,
            duration = obj.properties.duration
          }
        end
      end
    end
  end
  assert(self.steps[1], "initial path position missing")
  self.current_step_idx = 0
  self.deadline = love.timer.getTime()
end

function PathTracker:getPosition()
  return self.x, self.y, self.z
end

function PathTracker:nextStep(step)
  return step % #self.steps + 1
end

function PathTracker:update()
  local next_step_idx = self:nextStep(self.current_step_idx)
  local next_step = self.steps[next_step_idx]
  local t = love.timer.getTime()
  while t >= self.deadline do
    self.current_step_idx = next_step_idx
    next_step_idx = self:nextStep(next_step_idx)
    next_step = self.steps[next_step_idx]
    self.deadline = self.deadline + next_step.duration
  end
  local last_deadline = self.deadline - next_step.duration
  local progress = (t - last_deadline) / next_step.duration
  local cur_step = self.steps[self.current_step_idx]
  self.x = cur_step.x * (1 - progress) + next_step.x * progress
  self.y = cur_step.y * (1 - progress) + next_step.y * progress
  self.z = cur_step.z * (1 - progress) + next_step.z * progress
end

return PathTracker

