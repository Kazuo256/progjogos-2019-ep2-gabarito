
-- luacheck

local DEFS = require 'defs'

local TileSet = require 'class' ()

function TileSet:_init(data)
  self.data = data
  self.sheet = love.graphics.newImage(DEFS.MAPDIR .. data.image)
  self.sheet:setFilter('nearest', 'nearest')
  self.rows = data.imageheight / data.tileheight
  self.columns = data.imagewidth / data.tilewidth
  self.quads = {}
  for i = 1, self.rows do
    for j = 1, self.columns do
      local quad = love.graphics.newQuad((j - 1) * data.tilewidth,
                                         (i - 1) * data.tileheight,
                                         data.tilewidth, data.tileheight,
                                         data.imagewidth, data.imageheight)
      self.quads[self:index(i, j)] = quad
    end
  end
end

function TileSet:get(idx)
  return self.quads[idx - self.data.firstgid + 1]
end

function TileSet:index(i, j)
  return (i - 1) * self.columns + j
end

function TileSet:drawTile(id, x, y)
  local tw, th = self.data.tilewidth, self.data.tileheight
  return love.graphics.draw(self.sheet, self:get(id), x, y, 0, 1, 1,
                            math.floor(tw / 2), math.floor(th / 2))
end

return TileSet

