
-- luacheck: globals love

local Sprite = require 'class' ()

function Sprite.loadAll(layers_data)
  local sprites = {}
  for _, layer in ipairs(layers_data) do
    if layer.type == 'objectgroup' then
      for _, obj in ipairs(layer.objects) do
        if obj.type == 'sprite' then
          sprites[obj.id] = Sprite(obj)
        end
      end
    end
  end
  return sprites
end

function Sprite:_init(obj_data)
  self.spritesheet = love.graphics.newImage("chars/" .. obj_data.name .. ".png")
  self.spritesheet:setFilter('nearest', 'nearest')
  self.duration = 1 / obj_data.properties.fps
  self.flip = obj_data.properties.flip
  self.frames = {}
  for frame in obj_data.properties.frames:gmatch("%d+") do
    self.frames[#self.frames + 1] = tonumber(frame)
  end
  self.current = 1
  self.deadline = love.timer.getTime() + self.duration
  self.x = obj_data.x / obj_data.width
  self.y = obj_data.y / obj_data.height
  self.offsetx = obj_data.properties.offsetx
  self.offsety = obj_data.properties.offsety
  self.quads = {}
  local rows, cols = obj_data.properties.rows, obj_data.properties.columns
  local w, h = self.spritesheet:getDimensions()
  self.qw, self.qh = w/cols, h/rows
  for i = 1, rows do
    for j = 1, cols do
      self.quads[cols * (i - 1) + j] = love.graphics.newQuad(
        self.qw * (j - 1), self.qh * (i - 1), self.qw, self.qh, w, h
      )
    end
  end
end

function Sprite:getPosition()
  return self.x, self.y
end

function Sprite:update()
  local t = love.timer.getTime()
  while t >= self.deadline do
    self.current = self.current % #self.frames + 1
    self.deadline = self.deadline + self.duration
  end
end

function Sprite:draw(x, y)
  love.graphics.setColor(0, 0, 0, .2)
  love.graphics.ellipse('fill', x, y, self.qw/3, self.qh/6)
  love.graphics.setColor(1, 1, 1, 1)
  love.graphics.draw(self.spritesheet, self.quads[self.frames[self.current]],
                     x, y, 0, self.flip and -1 or 1, 1,
                     self.offsetx, self.offsety)
end

return Sprite

