
-- luacheck: globals love

local DEFS      = require 'defs'
local LOADDATA  = require 'loaddata'
local IsoView   = require 'isoview'

local _isoview

function love.load(args)
  assert(#args >= 1, "missing map path")
  local mapname = args[1]
  local data = LOADDATA(DEFS.MAPDIR .. mapname .. '.lua')
  _isoview = IsoView(data)
end

function love.update(_)
  _isoview:update()
end

function love.draw()
  _isoview:draw()
end

