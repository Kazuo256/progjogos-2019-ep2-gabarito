
-- luacheck: globals love

return function (path)
  local chunk = assert(love.filesystem.load(path))
  setfenv(chunk, {})
  return chunk()
end

